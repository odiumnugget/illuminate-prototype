﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using UnityEngine.XR;

public class VRSettings : MonoBehaviour
{
    [SerializeField]
    public bool VR_Enabled = true;
    public GameObject camRig, steamVR;
    public GameObject mainNonVRCam;

    private void Awake()
    {
        EnableDisableVR(VR_Enabled);
    }

    private void EnableDisableVR(bool _VR_Enabled)
    {
        if (_VR_Enabled)
        {
            XRSettings.enabled = true;
        }
        if (!_VR_Enabled)
        {
            XRSettings.enabled = false;
            XRSettings.LoadDeviceByName("None");
            camRig.SetActive(false);
            steamVR.SetActive(false);
            mainNonVRCam.SetActive(true);
        }
    }
}
