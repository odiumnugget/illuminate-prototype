﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VivePointer : MonoBehaviour
{
    public float _defaultLength = 5.0f;
    public GameObject _dot;

    private LineRenderer _linerenderer = null;

    private void Awake()
    {
        _linerenderer = GetComponent<LineRenderer>();
    }

    private void Update()
    {
        UpdateLine();
    }

    private void UpdateLine()
    {
        float targetLength = _defaultLength;

        RaycastHit hit = CreateRaycast(targetLength);

        Vector3 endPosition = transform.position + (transform.forward * targetLength);

        if(hit.collider != null)
        {
            endPosition = hit.point;
        }

        _dot.transform.position = endPosition;

        _linerenderer.SetPosition(0, transform.position);
        _linerenderer.SetPosition(1, endPosition);
    }

    private RaycastHit CreateRaycast(float length)
    {
        RaycastHit hit;

        Ray ray = new Ray(transform.position, transform.forward);
        Physics.Raycast(ray, out hit, _defaultLength);

        return hit;
    }
}
