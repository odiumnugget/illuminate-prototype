﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadialMenuItem : MonoBehaviour
{

    #region Variables
    [SerializeField] float scaleAmount = 1.35f;
    [SerializeField] float lerpRate = 3f;
    [SerializeField] AnimationCurve ScaleLerpCurve;
    [SerializeField] AnimationCurve FanOutCurve;

    [SerializeField] Transform origin;
    [SerializeField] Transform target;

    private Vector3 originalSize;
    [HideInInspector] public bool currentlySelected = false;
    
    #endregion

    private void Start()
    {
        originalSize = transform.localScale;
    }

    private void OnEnable()
    {
        transform.position = origin.position;
        StartCoroutine(FanOut());
    }

    private void OnDisable()
    {
        transform.localScale = originalSize;
    }

    public void Select()
    {
        StartCoroutine(ScaleUp());
        currentlySelected = true;
    }

    public void Deselect()
    {
        StartCoroutine(ScaleDown());
        currentlySelected = false;
    }

    private IEnumerator FanOut()
    {
        float counter = 0;
        while(counter < 1)
        {
            counter += Time.deltaTime * lerpRate;
            transform.position = Vector3.Lerp(origin.position, target.position, FanOutCurve.Evaluate(counter));
            yield return null;
        }
    }

    private IEnumerator ScaleUp()
    {
        float counter = 0;
        Vector3 targetScale = transform.localScale * scaleAmount;
        while(counter < 1)
        {
            counter += Time.deltaTime * lerpRate;
            transform.localScale = Vector3.Lerp(transform.localScale, targetScale, ScaleLerpCurve.Evaluate(counter));
            yield return null;
        }
    }

    private IEnumerator ScaleDown()
    {
        float counter = 0;
        while (counter < 1)
        {
            counter += Time.deltaTime * lerpRate;
            transform.localScale = Vector3.Lerp(transform.localScale, originalSize, ScaleLerpCurve.Evaluate(counter));
            yield return null;
        }
    }


}
