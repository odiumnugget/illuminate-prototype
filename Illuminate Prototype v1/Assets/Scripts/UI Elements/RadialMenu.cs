﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class RadialMenu : MonoBehaviour
{
    [SerializeField] private GameObject radial;
    [SerializeField] private GameObject controllerLeft;
    [SerializeField] private Camera VRcam;
    [SerializeField] private float rotationOffsetAngle;

    private Quaternion RotationYOffset;
    private Vector3 originPos;
    private Quaternion originRot;
    private bool active;
    private RadialMenuItem[] items;
     
    private void Awake()
    {
        radial.SetActive(false);        
    }

    private void Start()
    {
        //Menu Toggling
        originPos = radial.transform.localPosition;
        originRot = radial.transform.localRotation;

        //Menu Scrolling
        _currentScrollTime = 0;
        _currentScrollState = ScrollState.NONE;
        _scrollIndex = -1;
        
    }

    private void Update()
    {
        ToggleRadialMenu();
        
    }

    private void ToggleRadialMenu()
    {
        bool grabbing = GameManager.gm.input.GetGrabState();
        if (grabbing && !active)
        {
            radial.SetActive(true);            
            radial.transform.SetParent(null);                                                
            active = true;
        }
        else if (active && grabbing)
        {
            radial.transform.localPosition = controllerLeft.transform.position;
            radial.transform.LookAt(VRcam.transform);
            radial.transform.rotation *= Quaternion.Euler(rotationOffsetAngle, 0, 0);
            ScrollRadial();
        }
        else if (!grabbing && active)
        {
            radial.transform.SetParent(controllerLeft.transform);
            radial.transform.localPosition = originPos;
            radial.transform.localRotation = originRot;
            foreach (RadialMenuItem item in _menuItems)
            {
                item.Deselect();
            }
            radial.SetActive(false);
            active = false;
        }
    }


    #region Radial Scroll Variables

    [SerializeField] private RadialMenuItem[] _menuItems;
    [SerializeField] private float _trackpadDeadZone;
    [SerializeField] private float _timeDelay;
    private float _currentScrollTime;
    private enum ScrollState { LEFT, RIGHT, NONE}
    private ScrollState _currentScrollState;
    private int _scrollIndex;

    

    #endregion

    private float GetScrollDirection()
    {

        Vector2 trackpadAxis = GameManager.gm.input.GetTrackPadAxisState();
        float direction;
        if(trackpadAxis.x > _trackpadDeadZone && trackpadAxis.x != 0)
        {
            direction = trackpadAxis.x;
            return direction;
        }
        else if(trackpadAxis.x < -_trackpadDeadZone && trackpadAxis.x != 0)
        {
            direction = trackpadAxis.x;
            return direction;
        }
        else
        {
            direction = 0;
            return direction;
        }
        
    }

    private void ScrollRadial()
    {
        float x = GetScrollDirection();

        switch (_currentScrollState)
        {
            case ScrollState.LEFT:
                if(x < 0)
                {
                    ShouldSelectNextButton(-1);
                }
                else if(x > 0)
                {
                    SetState(ScrollState.RIGHT, _timeDelay);
                }
                else
                {
                    SetState(ScrollState.NONE, 0f);
                }
                break;
            case ScrollState.RIGHT:
                if(x > 0)
                {
                    ShouldSelectNextButton(1);
                }
                else if(x < 0)
                {
                    SetState(ScrollState.LEFT, _timeDelay);
                }
                else
                {
                    SetState(ScrollState.NONE, 0f);
                }
                break;
            case ScrollState.NONE:
                if(x > 0)
                {
                    SetState(ScrollState.RIGHT, _timeDelay);                                        
                }
                else if(x < 0)
                {
                    SetState(ScrollState.LEFT, _timeDelay);
                }
                break;
            default:
                break;
        }
    }

    private void ShouldSelectNextButton(int direction)
    {
        _currentScrollTime += Time.deltaTime;
        if(_currentScrollTime >= _timeDelay)
        {
            SelectNextButton(direction);
        }
    }

    private void SelectNextButton(int direction)
    {
        if(_scrollIndex != -1)
        {
            _menuItems[_scrollIndex].Deselect();
        }
        else if(_scrollIndex == -1 && direction == -1)
        {
            _scrollIndex = 0;
        }
        _scrollIndex = Mod((_scrollIndex + direction), _menuItems.Length);
        _menuItems[_scrollIndex].Select();
        
    }


    // To get positive value of array
    private static int Mod(int a, int b)
    {
        return (a % b + b) % b;
    }


private void SetState(ScrollState newState, float newTime)
    {
        _currentScrollState = newState;
        _currentScrollTime = newTime;
    }
}
