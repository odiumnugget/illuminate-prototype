﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridRenderer : MonoBehaviour
{
    [SerializeField]    private int gWidth, gHeight, borderSize;
    [SerializeField]    GameObject terrain;
    [SerializeField]    Color gridColor = Color.white;
    [SerializeField]    Color borderColor = Color.black;

    private void Start()
    {
        GenerateGrid();
    }

    void GenerateGrid()
    {
        Texture2D gridImage;
        Collider floorCollider;
        CreateGridTile(out gridImage, out floorCollider);
        DisplayGrid(gridImage, floorCollider);

    }

    private void DisplayGrid(Texture2D gridImage, Collider floorCollider)
    {
        gridImage.wrapMode = TextureWrapMode.Repeat;
        terrain.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", gridImage);
        terrain.GetComponent<MeshRenderer>().material.SetTextureScale("_MainTex", new Vector2(floorCollider.bounds.size.x, floorCollider.bounds.size.z));
    }

    private void CreateGridTile(out Texture2D gridImage, out Collider floorCollider)
    {
        gridImage = new Texture2D(gWidth, gHeight);
        MeshRenderer renderer = GetComponent<MeshRenderer>();

        floorCollider = terrain.GetComponent<Collider>();
        //    Vector3 foorSize = new Vector3(floorCollider.bounds.size.x, floorCollider.bounds.size.z);

        for (int x = 0; x < gridImage.width; x++)
        {
            for (int y = 0; y < gridImage.height; y++)
            {
                if (x < borderSize || x > gridImage.width - borderSize || y < borderSize || y > gridImage.height - borderSize)
                {
                    gridImage.SetPixel(x, y, new Color(borderColor.r, borderColor.g, borderColor.b, 1));
                }
                else
                {
                    gridImage.SetPixel(x, y, new Color(gridColor.r, gridColor.g, gridColor.b, 1));
                }
            }
            gridImage.Apply();
        }
    }
}
