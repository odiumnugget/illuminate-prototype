﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class InputManager : MonoBehaviour
{
    public VRSettings _settings;
    public ViveInput _viveInput;
    public MouseInput _mouseInput;

    public Ray InteractRay()   
    {
        if (!_settings.VR_Enabled)
        {
            return _mouseInput.InteractRay();
        }
        else
        {
            return _viveInput.InteractRay();
        }

    }

    public bool GetInteractDown()
    {
        if (!_settings.VR_Enabled)
        {
            return _mouseInput.GetInteractDown();
        }

        else
        {
            return _viveInput.GetInteractDown();            
        }
    }
    

    public bool GetGrabDown()
    {
        if (!_settings.VR_Enabled)
        {
            return _mouseInput.GetGrabDown();
        }
        else
        {
            return _viveInput.GetGrabDown();
        }
    }

    public bool GetGrabState()
    {
        
        if (!_settings.VR_Enabled)
        {
            return _mouseInput.GetGrabState();
        }
        else
        {        
            return _viveInput.GetGrabState();
        }
    }

    public Vector2 GetTrackPadAxisState()
    {
        return viveInput.GetTouchAxisState();
    }
}
