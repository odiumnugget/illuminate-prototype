﻿using UnityEngine;
using System.Collections;
using Valve.VR;

public class ViveInput : MonoBehaviour
{
    public SteamVR_Input_Sources rightHand;
    public SteamVR_Input_Sources leftHand;
    public SteamVR_Action_Boolean interact;
    public SteamVR_Action_Boolean grabGrip;
    public SteamVR_Action_Vector2 trackpad;
    public GameObject rightController;
    
    

    public Ray InteractRay()
    {
        Debug.Log("InteractRay called");        
        return new Ray(rightController.transform.position, rightController.transform.forward);
    }

    public bool GetInteractDown()
    {
        return interact.GetLastStateDown(rightHand);

    }

    public bool GetGrabDown()
    {
        return grabGrip.GetLastStateDown(leftHand);
    }

    public bool GetGrabState()
    {
        Debug.Log(grabGrip.GetState(leftHand));
        return grabGrip.GetState(leftHand);
    }

    public Vector2 GetTouchAxisState()
    {
        Vector2 trackpadOutput;
        trackpadOutput.y = trackpad.axis.y;
        trackpadOutput.x = trackpad.axis.x;
        return trackpadOutput;
    }

}
