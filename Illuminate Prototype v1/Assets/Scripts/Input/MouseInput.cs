﻿using UnityEngine;
using System.Collections;

public class MouseInput : MonoBehaviour
{ 
    public Ray InteractRay()
    {
        return Camera.main.ScreenPointToRay(Input.mousePosition);
    }


    public bool GetInteractDown()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            return true;
        }
        else return false;
    }

    public bool GetGrabDown()
    {
        if (Input.GetButtonDown("Fire2"))
        {
            return true;
        }
        else return false;
    }

    public bool GetGrabState()
    {
        if (Input.GetButton("Fire2"))
        {
            return true;
        }
        else return false;
    }
}
