﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionGUIRenderer : MonoBehaviour
{

    [SerializeField]    private GameObject _projector;
    [SerializeField]    public LineRenderer _lineRenderer;
    [SerializeField]    private GridManager _gridManager;

    private Unit _unit;
    

    

    private void Start()
    {
        _unit = GetComponent<Unit>();
        _unit.OnSelectChanged += SpawnSelectionCircle;
    }

    

    private void SpawnSelectionCircle(bool isSelected)
    {
        _projector.SetActive(isSelected);
    }

    public void DrawMovementIndicator(Node target) 
    {
        
        Vector3 heightOffset = Vector3.up * 0.01f; 
        _lineRenderer.enabled = true;
        _lineRenderer.SetPosition(0, _gridManager.NodeFromWorldPoint(transform.position)._worldPosition + heightOffset);
        {
            _lineRenderer.SetPosition(1, target._worldPosition + heightOffset);                                                        
        }
    }


}
