﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Singleton
    public static GameManager gm;
    private void Awake()
    {
        if(gm == null)
        {
            gm = this;
        }
        
        if(gm != this)
        {
            Destroy(gameObject);
        }
    }

    #endregion


    public GridManager grid;
    public InputManager input;
    public ActionsManager actions;




}
