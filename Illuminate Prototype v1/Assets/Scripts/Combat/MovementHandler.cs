﻿using UnityEngine;
using System;
using System.Collections;

public enum MovementPhase
{
    SELECTIONINDICATOR,
    MOVE
}


public class MovementHandler : MonoBehaviour
{

    private GridManager _grid;
    private InputManager _input;

    [SerializeField] private float _unitMoveLerpSpeed = 4f;
    [SerializeField] private AnimationCurve _lerpAnimCurve;

    private MovementPhase _currentMovementPhase;

    private void Start()
    {
        _grid = GameManager.gm.grid;
        _input = GameManager.gm.input;
    }


    public void Move(GameObject unitToMove, Action onCompleteCallback)
    {

        Node target = GetTargetPosition();
        switch (_currentMovementPhase)
        {
            case MovementPhase.SELECTIONINDICATOR:
                Debug.Log("Attempt Draw Indicator");
                unitToMove.GetComponent<SelectionGUIRenderer>().DrawMovementIndicator(target);
                if (_input.GetInteractDown())
                {
                    _currentMovementPhase = MovementPhase.MOVE;
                }
                break;
            case MovementPhase.MOVE:
                
                if (target != null)
                {
                    StartCoroutine(LerpUnitPosition(unitToMove, target));
                    unitToMove.GetComponent<SelectionGUIRenderer>()._lineRenderer.enabled = false;
                    onCompleteCallback();
                }
                break;
            default:
                break;
        }
    }

    private IEnumerator LerpUnitPosition(GameObject unitToMove, Node target)
    {        
        Vector3 unitStartingPosition = unitToMove.transform.position;
        float counter = 0;
        while (counter < 1)
        {
            Debug.Log("Lerping");
            counter += Time.deltaTime * _unitMoveLerpSpeed;
            unitToMove.transform.position = Vector3.Lerp(unitStartingPosition, target._worldPosition, _lerpAnimCurve.Evaluate(counter));
            yield return null;
        }
        _currentMovementPhase = MovementPhase.SELECTIONINDICATOR;
    }

    public Node GetTargetPosition()
    {
        Node targetNode;
        Ray ray = _input.InteractRay();
        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            Debug.Log("Raycast out");
            targetNode = _grid.NodeFromWorldPoint(hit.point);
        }
        else
        {
            targetNode = null;
        }
        return targetNode;
    }
}
