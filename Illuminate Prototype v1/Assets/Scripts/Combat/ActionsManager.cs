﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ActionState
{
    SELECT,
    MOVE,
    ATTACK,
    ACTION
}

public class ActionsManager : MonoBehaviour
{

    [SerializeField] private MovementHandler _movementHandler;
    private InputManager _input;

    private ActionState _currentActionState;
    private GameObject _selectedUnit;

    private void Start()
    {
        _input = GameManager.gm.input;
    }

    private void Update()
    {
        switch (_currentActionState)
        {
            case ActionState.SELECT:
                if (_input.GetInteractDown())
                {
                    SelectUnit(_input.InteractRay());
                    Debug.Log("Interact pressed");                    
                }
                break;
            case ActionState.MOVE:
                _movementHandler.Move(_selectedUnit, SetSelectState);
                break;
            case ActionState.ATTACK:
                break;
            case ActionState.ACTION:
                break;
            default:
                break;
        }
    }

    private void SetSelectState()
    {
        _currentActionState = ActionState.SELECT;
    }


    //private void SelectUnit()
    //{
    //    if (Input.GetButtonDown("Fire1"))
    //    {
    //        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
    //        if (Physics.Raycast(ray, out RaycastHit hit))
    //        {
    //            if (hit.collider.tag == "Miniature")
    //            {
    //                if (selectedUnit != null) { DeselectUnit(selectedUnit); }
    //                selectedUnit = hit.collider.gameObject;
    //                selectedUnit.GetComponent<SelectionGUIRenderer>().isSelected = true;
    //                currentActionState = ActionState.MOVE;
    //            }

    //            else if (selectedUnit != null)
    //            {
    //                DeselectUnit(selectedUnit);
    //            }
    //        }
    //    }
    //}

    private void SelectUnit(Ray ray)
    {
        if(Physics.Raycast(ray, out RaycastHit hit))
        {                         
            DeselectUnit(_selectedUnit);
            ISelectable unit = hit.collider.GetComponent<ISelectable>();
            if(unit != null)
            {
                unit.Select();
                _selectedUnit = hit.collider.gameObject;
                _currentActionState = ActionState.MOVE;
            }
        }
    }


    private void DeselectUnit(GameObject unitToDeselect)
    {
        if (_selectedUnit != null)
        {
            _selectedUnit.GetComponent<ISelectable>().Deselect();
        }
        _selectedUnit = null;
        _currentActionState = ActionState.SELECT;
    }

    
}
