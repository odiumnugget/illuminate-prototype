﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;


public class Unit : MonoBehaviour, ISelectable
{
    private bool _selected;

    public delegate void SelectChangedHandler(bool selected);
    public event SelectChangedHandler OnSelectChanged;
    public bool Selected
    {
        get { return _selected; }
        set
        {
            _selected = value;
            OnSelectChanged(Selected);
        }
    }
    

    public void Deselect()
    {
        Selected = false;
    }

    public void Select()
    {
        Selected = true;
    }

    private void Update()
    {
        
    }

}
